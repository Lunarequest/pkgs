import requests
import os, sys

key = os.environ.get("API_KEY")
sandbox = os.environ.get("SANDBOX_URL")
recipient = os.environ.get("RECIPIENT")
if key and sandbox and recipient:
    url = f"https://api.mailgun.net/v3/{sandbox}/messages"
    request = requests.post(
        url,
        auth=("api", key),
        data={
            "from": "update_your_shit_luna@nullrequest.com",
            "to": recipient,
            "subject": "Update linux clang",
            "text": f'new version has been released {os.environ.get("LINUX_VERSION")}',
        },
    )
    print(f"status {request.status_code}\nBody: {request.text}")
else:
    print("One or more required env vars are missing")
    sys.exit(1)
