#!/usr/bin/env bash

_getlatestlinux() {
	curl https://api.github.com/repos/archlinux/linux/git/refs/tags | jq ".[-1].ref" | sed "s/refs\/tags\/v//" | sed 's/"//g' |  sed 's/-/./g'
}
export LINUX_VERSION=`_getlatestlinux`

_releaseIfNotExists() {
   if [ $(git tag -l $LINUX_VERSION) ]; then
		echo $LINUX_VERSION has already been release
	else
		python updatemail.py
  fi
}

echo found linux version $LINUX_VERSION
_releaseIfNotExists
